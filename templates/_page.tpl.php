<?php 
$blockify = variable_get('blockify_blocks', array());
$brand = render($page['brand']); 
$navigation = render($page['navigation']);
$header = render($page['header']); 
?>

<div id="page">
  
  <header id="header" role="banner">
    <?php print render($page['header']); ?>
    
    <?php if ($top_bar): ?>
      <section class="top-bar">
        <ul class="title-area">
          <li id="site-name" class="name header__site-name">
    <?php endif; ?>
    <?php if ($site_name && empty($blockify['blockify-site-name'])): ?>
      <h1 class="header__site-name" id="site-name"><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" class="header__site-link" rel="home"><span><?php print $site_name; ?></span></a></h1>
    <?php endif; ?>
    <?php if (!$brand && $site_slogan): ?>
      <p class="header__site-slogan" id="site-slogan"><?php print $site_slogan; ?></p>
    <?php endif; ?>

    <?php if ($top_bar): ?>
          </li>
          <li class="toggle-topbar menu-icon"><a href="#"><span><?php print $top_bar_menu_text; ?></span></a></li>
        </ul>
      <section class="top-bar-section">
    <?php endif; ?>
    
    <?php print $navigation; ?>
    
    <?php if (!$navigation && $main_menu): ?>
      <?php $main_menu_output = menu_tree_output(menu_tree_all_data(variable_get('menu_main_links_source', 'main-menu')));?>
      <nav id="main-menu" role="navigation" tabindex="-1">
        <h2 class="element-invisible"><?php print t('Main menu'); ?></h2>
        <ul class="left">
          <?php print _bootstart_menu_tree($main_menu_output); ?>
        </ul>
      </nav>
    <?php endif; ?>
    
    <?php if (!$navigation && $secondary_menu): ?>
      <?php $secondary_menu_output = menu_tree_output(menu_tree_all_data(variable_get('menu_secondary_links_source', 'user-menu'))); ?>
      <nav class="header__secondary-menu" id="secondary-menu" role="navigation">
        <h2 class="element-invisible"><?php print $secondary_menu_heading; ?></h2>
        <ul class="right">
        <?php print _bootstart_menu_tree($secondary_menu_output) ?>
        </ul>
      </nav>
    <?php endif; ?>
  
    <?php if ($top_bar): ?>
      </section> <!-- // .top-bar-section -->
    </section> <!-- // .top-bar -->
    <?php endif; ?>
  
  </header>


  <?php if (!empty($page['featured'])): ?>
    <div id="featured">
      <?php print render($page['featured']); ?>
    </div>
  <?php endif; ?>
    
  <main id="main" role="main">
    <?php if (!empty($page['before_main'])): ?>
      <div id="before-main">
        <?php print render($page['before_main']); ?>
      </div>
    <?php endif; ?>

    <div id="content">
      <?php print render($page['highlighted']); ?>
      <?php if (empty($blockify['blockify-breadcrumb']) && theme_get_setting('zen_breadcrumb') == 'yes') print $breadcrumb; ?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php if ($title && empty($blockify['blockify-page-title'])): ?>
        <h1 class="page__title title" id="page-title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php if (empty($blockify['blockify-messages'])) print $messages; ?>
      <?php if (empty($blockify['blockify-tabs'])) print render($tabs); ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links && empty($blockify['blockify-actions'])): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php if (empty($blockify['blockify-feed-icons'])) print $feed_icons; ?>
    </div> <!-- // #content -->
    
    <?php
      // Render the sidebars to see if there's anything in them.
      $sidebar_first  = render($page['sidebar_first']);
      $sidebar_second = render($page['sidebar_second']);
    ?>
    <?php if ($sidebar_first || $sidebar_second): ?>
    <aside class="sidebars">
      <?php if ($sidebar_first): ?>
        <section id="sidebar-first">
          <?php print $sidebar_first; ?>
        </section>
      <?php endif; ?>
      <?php if ($sidebar_second): ?>
        <section id="sidebar-second">
          <?php print $sidebar_second; ?>
        </section>
      <?php endif; ?>
    </aside><!-- // .sidebars -->
    <?php endif; ?>
  
  </main> <!-- // #main -->
  
  <?php if (!empty($page['footer'])): ?> 
  <footer id="footer">
    <?php print render($page['footer']); ?>
  </footer><!-- // #footer -->
  <?php endif; ?>

</div><!-- // #page -->

<?php print render($page['bottom']); ?>
