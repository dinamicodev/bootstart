<?php
/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 */
function bootstart_form_system_theme_settings_alter(&$form, &$form_state) {
  if (!isset($form['bootstart'])) {
    $form['bootstart'] = array(
      '#type' => 'vertical_tabs',
      '#weight' => -10,
    );

    /**
     * General settings.
     */
    $form['bootstart']['general'] = array(
      '#type' => 'fieldset',
      '#title' => t('General Settings'),
    );
    
    $form['bootstart']['general']['theme_settings'] = $form['theme_settings'];
    unset($form['theme_settings']);

    /*$form['bootstart']['general']['logo'] = $form['logo'];
    unset($form['logo']);

    $form['bootstart']['general']['favicon'] = $form['favicon'];
    unset($form['favicon']);*/

    /**
     * bootstrap settings.
     */
    $form['bootstart']['bootstrap'] = array(
      '#type' => 'fieldset',
      '#title' => t('bootstrap Settings'),
    );

    $form['bootstart']['bootstrap']['top_bar'] = array(
      '#type' => 'fieldset',
      '#title' => t('Top Bar'),
      '#description' => t('The bootstrap Top Bar gives you a great way to display a complex navigation bar on small or large screens.'),
    );

    $form['bootstart']['bootstrap']['top_bar']['bootstart_top_bar_enable'] = array(
      '#type' => 'select',
      '#title' => t('Enable'),
      '#description' => t('If enabled, the site name and main menu will appear in a bar along the top of the page.'),
      '#options' => array(
        0 => t('Never'),
        1 => t('Always'),
        2 => t('Mobile only'),
      ),
      '#default_value' => theme_get_setting('bootstart_top_bar_enable'),
    );

    $form['bootstart']['bootstrap']['top_bar']['bootstart_top_bar_animate'] = array(
      '#type' => 'textfield',
      '#title' => t('Animation'),
      '#description' => t('Specify an animation for the top bar or leave blank for none.'),
      '#default_value' => theme_get_setting('bootstart_top_bar_animate'),
      '#states' => array(
        'visible' => array(
          'select[name="bootstart_top_bar_enable"]' => array('!value' => '0'),
        ),
      ),
    );

    $form['bootstart']['bootstrap']['top_bar']['bootstart_top_bar_grid'] = array(
      '#type' => 'checkbox',
      '#title' => t('Contain to grid'),
      '#description' => t('Check this for your top bar to be set to your grid width.'),
      '#default_value' => theme_get_setting('bootstart_top_bar_grid'),
      '#states' => array(
        'visible' => array(
          'select[name="bootstart_top_bar_enable"]' => array('!value' => '0'),
        ),
      ),
    );

    $form['bootstart']['bootstrap']['top_bar']['bootstart_top_bar_sticky'] = array(
      '#type' => 'checkbox',
      '#title' => t('Sticky'),
      '#description' => t('Check this for your top bar to stick to the top of the screen when the user scrolls down.'),
      '#default_value' => theme_get_setting('bootstart_top_bar_sticky'),
      '#states' => array(
        'visible' => array(
          'select[name="bootstart_top_bar_enable"]' => array('!value' => '0'),
        ),
      ),
    );

    $form['bootstart']['bootstrap']['top_bar']['bootstart_top_bar_menu_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Menu text'),
      '#description' => t('Specify text to go beside the mobile menu icon or leave blank for none.'),
      '#default_value' => theme_get_setting('bootstart_top_bar_menu_text'),
      '#states' => array(
        'visible' => array(
          'select[name="bootstart_top_bar_enable"]' => array('!value' => '0'),
        ),
      ),
    );
  }
}
