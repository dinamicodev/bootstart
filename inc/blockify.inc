<?php

/**
 * Returns the rendered site name.
 *
 * @ingroup themeable
 */
function bootstart_blockify_site_name($vars) {
  $name = $vars['site_name'];
  $is_front = drupal_is_front_page();

  $output = $is_front ? '<h1' : '<p';
  $output .= ' id="site-name"';
  //$output .= ' class="header__site-name"';
  $output .= '>';
  $output .= l('<span>' . $name . '</span>', '', array('attributes' => array('title' => t('Home'), 'rel' => 'home'), 'html' => TRUE));
  $output .= $is_front ? '</h1>' : '</p>';
  
  return $output;
}

/**
 * Returns the rendered site slogan.
 *
 * @ingroup themeable
 */
function bootstart_blockify_site_slogan($variables) {
  return '<p id="site-slogan">' . $variables['site_slogan'] . '</p>';
}


/*function bootstart_preprocess_blockify_page_title(&$variables) { //dpm($variables);
  
  // Get Display Suite Custom page title settings and hide title if necessary.
  // $entity->system_entity_type is not a standard drupal property
  // you need systemplus module for this to work.
  if (($entity = menu_get_object()) && isset($entity->system_entity_type)) { //dpm($entity); dpm(get_class($entity)); 
      
    $entity_type = $entity->system_entity_type;
    $wrapper = entity_metadata_wrapper($entity_type, $entity);
    
    $bundle = $wrapper->getBundle();
    $view_mode = 'full';
    
    if ($overridden_view_mode = ds_extras_get_view_mode()) {
      $view_mode = $overridden_view_mode;
    }
    $layout = ds_get_layout($entity_type, $bundle, $view_mode);
    
    if (variable_get('ds_extras_hide_page_title', FALSE)) {
      if (isset($layout['settings']['hide_page_title']) && $layout['settings']['hide_page_title'] == 1 /*&& ds_extras_is_entity_page_view($build, $entity_type)*//*) {
        $variables['page_title'] = '';
      }
    }
  }
}*/

/**
 * Returns the rendered page title.
 *
 * @ingroup themeable
 */
function bootstart_blockify_page_title($variables) {
  if (!empty($variables['page_title'])) {
    $title_attributes_array = array(
      'class' => array('title'),
      'id' => array('page-title'),
    );
    $title_attributes = drupal_attributes($title_attributes_array);

    return '<h1' . $title_attributes . '>' . $variables['page_title'] . '</h1>';
  }
}

/*
  Return the type of a given entity.

  @param $entity
    an entity like node, user, taxonomy term etc.

  @return
     A string containing the entity type's name.
  */
function _bootstart_entity_type_get_name($entity) {

    if (empty($entity) || ! is_object($entity))
        return FALSE;

    static $drupal_static_fast;
    if (!isset($drupal_static_fast)) {
        $drupal_static_fast = &drupal_static(__FUNCTION__);
    }
    
    $entity_class = get_class($entity);
    $entity_bundle = $entity->type;
    if ( ! isset($drupal_static_fast[$entity_class]) ||  ! isset($drupal_static_fast[$entity_class][$entity_bundle]) ) {

        $detected_type = FALSE;

        $reflectionClass = new \ReflectionClass($entity_class); 
        foreach ($reflectionClass->getProperties() as $property) { dpm($property);

            if ($property->getName() == 'entityType') {
                $property->setAccessible(TRUE);
                $detected_type = $property->getValue($entity);
                $property->setAccessible(FALSE);
            }
        }

        if ( ! isset($drupal_static_fast[$entity_class]))
            $drupal_static_fast[$entity_class] = array();

        $drupal_static_fast[$entity_class][$entity_bundle] = $detected_type;

    }


    return $drupal_static_fast[$entity_class][$entity_bundle];
}