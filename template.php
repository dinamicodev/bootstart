<?php
/*
 * @file
 */

#require_once('inc/elements.inc');
#require_once('inc/form.inc');
require_once('inc/menu.inc');
require_once('inc/theme.inc');
require_once('inc/blockify.inc');
 
/**
 * Implements hook_css_alter()
 */
function bootstart_css_alter(&$css) {
  // Remove defaults.css file.
  //dsm(drupal_get_path('module', 'system') . '/system.menus.css');
  #unset($css[drupal_get_path('module', 'system') . '/system.menus.css']);
}

/**
 * Implements hook_js_alter()
 */
function bootstart_js_alter(&$js) {
}


function bootstart_preprocess_datatables_view(&$vars) {
  $theme_path = drupal_get_path('theme', 'bootstart');
  drupal_add_css($theme_path . '/css/datatables.css');

  #$lib_path = _datatables_get_path(); #dpm($lib_path); 
  #drupal_add_css($lib_path . '/media/css/jquery.dataTables.css');
}

/**
 * Implements hook_html_head_alter().
 */
function bootstart_html_head_alter(&$head_elements) {
  // HTML5 charset declaration.
  /*$head_elements['system_meta_content_type']['#attributes'] = array(
    'charset' => 'utf-8',
  );*/
  
  // Force IE to use Chrome Frame if installed.
  $head_elements['chrome_frame'] = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'content' => 'ie=edge, chrome=1',
      'http-equiv' => 'x-ua-compatible',
    ),
  );
  
  // Remove image toolbar in IE.
  $head_elements['ie_image_toolbar'] = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => 'ImageToolbar',
      'content' => 'false',
    ),
  );
}

/**
 * Implements hook_preprocess_block()
 */
function bootstart_preprocess_block(&$variables) {;
  $block = $variables['block'];
  
  // Panels pane
  if (isset($variables['pane'])) {
    $variables['pane'] = $variables['elements']['#pane'];
  }

  if (isset($block->css_class)) {
    $variables['classes_array'][] = $block->css_class;
  }
}

/**
 * Implements template_preprocess_field().
 */
function bootstart_preprocess_field(&$variables) { #kpr($variables); exit;
  
  if (!in_array('field', $variables['classes_array'])) {
    array_unshift($variables['classes_array'], 'field');
  }
}

/**
 * Implements template_preprocess_html().
 */
function bootstart_preprocess_html(&$variables) {

  // Add Foundation scripts
  /*$theme_path = drupal_get_path('theme', 'bootstart');
  $bootstart_scripts_list = theme_get_setting('bootstart_scripts'); #dpm($bootstart_scripts_list);
  
  if (!empty($bootstart_scripts_list) && is_array($bootstart_scripts_list)) {
    foreach ($bootstart_scripts_list as $script) {
      drupal_add_js($theme_path . '/lib/foundation5/js/foundation/'. $script);
    }
  }

  // Init Foundation javascript components
  if (theme_get_setting('bootstart_scripts_init')) {
    drupal_add_js('jQuery(document).foundation();', array('type' => 'inline', 'scope' => 'footer', 'weight' => 5));
  }*/
}

/**
 * Implements template_preprocess_page
 */
function bootstart_preprocess_page(&$variables) { #dpm($variables);
  
  // Add page--node_type.tpl.php suggestions
  if (!empty($variables['node'])) {
    $variables['theme_hook_suggestions'][] = 'page__' . $variables['node']->type;
  }
}


/**
 * Implements template_preprocess_node
 */
function bootstart_preprocess_node(&$variables) { #dpm($variables);
  // Add node--node_type--view_mode.tpl.php suggestions
  $variables['theme_hook_suggestions'][] = 'node__' . $variables['type'] . '__' . $variables['view_mode'];

  // Add node--view_mode.tpl.php suggestions
  $variables['theme_hook_suggestions'][] = 'node__' . $variables['view_mode'];
}


/**
 * Override or insert variables into the panels-pane templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
function bootstart_preprocess_panels_pane(&$variables, $hook) {
  $pane = $variables['pane'];

  if (isset($pane->configuration['display'])) {
    $variables['classes_array'][] = 'pane-display-id-' . $pane->configuration['display'];
  }
}




